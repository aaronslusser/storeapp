//
//  TopApps.swift
//  GridSearch
//
//  Created by  Amber  on 11/23/21.
//

import SwiftUI
import Kingfisher

struct RSS: Decodable {
    let feed: Feed
}

struct Feed: Decodable {
    let results: [Result]
}

struct Result: Decodable, Hashable {

    
    let artistName, name, artworkUrl100, releaseDate: String
}

class GridViewModel: ObservableObject {
    @Published var items = 0..<5
    @Published var results = [Result]()
    @Published var isDownloading = true
    init() {
            guard let url = URL(string: "https://rss.applemarketingtools.com/api/v2/us/apps/top-free/50/apps.json") else { return }
            URLSession.shared.dataTask(with: url) { data, resp, err in
                guard let data = data else { return }
                do{
                    let rss = try JSONDecoder().decode(RSS.self, from: data)
                    print(rss)
                    self.results = rss.feed.results
                    self.isDownloading = false
                } catch {
                    print("Failed to decode: \(error)")
                    self.isDownloading = false
                }
            }.resume()
    }
}

struct TopApps: View {
    
    @ObservedObject var vm: GridViewModel
    
    @State var searchText = ""
    @State var isSearching = false
    
    var body: some View {
        NavigationView {
            ScrollView {
                HStack {
                    TextField("Search terms", text: $searchText)
                        .padding(.leading, 24)
                }
                .padding()
                .background(Color(.systemGray5))
                .cornerRadius(12)
                .padding(.horizontal)
                .onTapGesture(perform: {
                    isSearching = true
                })
                .overlay(
                    HStack{
                        Image(systemName: "magnifyingglass")
                        Spacer()
                        if isSearching{
                            Button {
                                searchText = ""
                            } label: {
                                Image(systemName: "xmark.circle.fill")
                                    .padding(.vertical)
                            }

                            
                        }
                        
                    }.padding(.horizontal, 32)
                        .foregroundColor(.gray)
                )
                if vm.isDownloading {
                    HStack{
                        Spacer()
                        ProgressView("Loading").progressViewStyle(CircularProgressViewStyle(tint: .gray))
                        Spacer()
                    }
                }
                
                LazyVGrid(columns: [
                    GridItem(.flexible(minimum: 50, maximum: 200), spacing: 16, alignment: .top),
                    GridItem(.flexible(minimum: 50, maximum: 200), spacing: 16, alignment: .top),
                    GridItem(.flexible(minimum: 50, maximum: 200), spacing: 16),
                ], alignment: .leading, spacing: 16, content: {
                    ForEach(vm.results.filter({ "\($0)".contains(searchText) || searchText.isEmpty }), id: \.self) { app in
                        AppInfo(app: app)
                    }
                }).padding(.horizontal, 12)
                    .padding(.top, 12)
            }.navigationTitle("Top 50 Free")
        }
    }
}

struct AppInfo: View {
    
    let app: Result
    
    var body: some View {
        VStack(alignment: .leading, spacing: 4) {
            KFImage(URL(string: app.artworkUrl100))
                .resizable()
                .scaledToFit()
                .cornerRadius(22)
            Text(app.name)
                .font(.system(size: 10, weight: .semibold))
                .padding(.top, 4)
            Text(app.releaseDate)
                .font(.system(size: 9, weight: .regular))
            Text(app.artistName)
                .font(.system(size: 9, weight: .regular))
                .foregroundColor(.gray)
            Spacer()
        }
    }
}

struct TopApps_Previews: PreviewProvider {
    static var previews: some View {
        TopApps(vm: GridViewModel())
    }
}
