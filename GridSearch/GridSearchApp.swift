import SwiftUI

@main
struct GridSearchApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
