import SwiftUI
import Kingfisher

class BooksViewModel: ObservableObject {
    @Published var items = 0..<5
    @Published var results = [Result]()
    @Published var isDownloading = true
    init() {
            guard let url = URL(string: "https://rss.applemarketingtools.com/api/v2/us/books/top-free/50/books.json") else { return }
            URLSession.shared.dataTask(with: url) { data, resp, err in
                guard let data = data else { return }
                do{
                    let rss = try JSONDecoder().decode(RSS.self, from: data)
                    print(rss)
                    self.results = rss.feed.results
                    self.isDownloading = false
                } catch {
                    print("Failed to decode: \(error)")
                    self.isDownloading = false
                }
            }.resume()
    }
}

struct BooksView: View {
    @ObservedObject var vm: BooksViewModel
    @State var searchText = ""
    
    private func getScale(proxy: GeometryProxy) -> CGFloat {
        var scale: CGFloat = 1
        
        let x = proxy.frame(in: .global).minX
        
        let diff = abs(x - 32)
        if diff < 100 {
            scale = 1 + (100 - diff) / 500
        }
        
        return scale
    }
    
    var body: some View {
        NavigationView {
            ScrollView {
                
                ScrollView(.horizontal) {
                    HStack(spacing: UIScreen.main.bounds.width/3 - 50) {
                        ForEach(vm.results.filter({ "\($0)".contains(searchText) || searchText.isEmpty }), id: \.self) { book in
                            GeometryReader { proxy in
                                
                                NavigationLink(
                                    destination: KFImage(URL(string: book.artworkUrl100)).frame(alignment:.center),
                                    label: {
                                        VStack {
                                            let scale = getScale(proxy: proxy)
                                            
                                            KFImage(URL(string: book.artworkUrl100))
                                                .resizable()
                                                .scaledToFill()
                                                .frame(width: 150,alignment: .center)
                                                .overlay(
                                                    RoundedRectangle(cornerRadius: 5).stroke(lineWidth: 0.5)
                                                )
                                                .clipped()
                                                .cornerRadius(5)
                                                .shadow(radius: 5)
                                                .scaleEffect(CGSize(width: scale, height: scale))
                                                .animation(.easeOut(duration: 0.5))
                                                
                                                
                                            
                                            Text(book.name)
                                                .padding(.top,33)
                                                .multilineTextAlignment(.center)
                                                .foregroundColor(Color(.label))
                                        }
                                        .frame(alignment: .center)
                                        //.background(Color.red)
                                    })
                            }
                            .frame(width: 125, height:300, alignment: .center)
                            //.background(Color.blue)
                            .padding(.leading, 15)
                            
                            
                            
                                
                        }
                    }.padding(32)
                    
                }
                
                
                
            }.navigationTitle("Books Carousel")
        }
    }
}

struct BooksView_Previews: PreviewProvider {
    static var previews: some View {
        BooksView(vm: BooksViewModel())
    }
}
